import { Component, OnInit, HostListener, AfterViewInit } from '@angular/core';
import { MatSnackBar, MatTabGroup } from "@angular/material";
import { CategoryProductService } from "../../services/category-product.service";
import { ProductService } from "../../services/product.service";
import { CategoryProduct, CategoryProductEdit } from "../../models/category-product";
import { Product, ProductEdit } from "../../models/product";
import { EditFactor } from '../../models/factor';
import { EditFactorDetail } from '../../models/factor-detail';
import { SettingService } from '../../services/setting.service';
import { Setting } from '../../models/setting';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit,AfterViewInit{

  counterDown: number = 15
  interval;
  isLoading = false;
  isProductLoading = false;
  isGetData = true;
  
  categoryProducts: CategoryProduct[] = []
  categoryProductsEdit: CategoryProductEdit[] = []
  categoryProductsResult: CategoryProductEdit[] = []
  flagCategoryProduct = false;
  factor: EditFactor = {_id: "", addressId: "", discountCodeId: "", paymentOnline: false, factorDetails:[]}
  product: ProductEdit = {_id: "", categoryProduct: null, count: 0, imageUrl: "", ingredients:[] ,price: "",discount: "",priceWithDiscount: "", priority: 0, title: "",available:true};
  setting: Setting = {_id: "", minOrder: 0, packing: 0, supportNumber: "", address: "", tax: 9, fromTime: 0, toTime: 0, isStoreOpen: false};
  flagSetting = false;
  totalFactor: String = "0";
  totalFactorNumber: number = 0;
  isIngredient: boolean = false;

  curentCategoryId = "";

  scrHeight:any;
  scrWidth:any;
  imgHeight:any;

  ingeradientHeight = 0;
  selectedTab = 0;
  lastScroll = 0;
  flagEndScroll = false;
  lastSkipNumber = 0;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
  }
  constructor( private service: CategoryProductService, private serviceProduct: ProductService, private settingService: SettingService, private snackBar: MatSnackBar) {
    this.getScreenSize();
    this.factor = JSON.parse(localStorage.getItem('factor')) as EditFactor

    this.startCountdown()
    this.getSetting();
    this.getCategoryProduct();
    
   }

   getSetting(){
     if(!this.flagSetting){
      if (localStorage.getItem('setting')) {
        this.setting = JSON.parse(localStorage.getItem('setting')) as Setting
        this.flagSetting = true;
        this.checkStop();
      }else{
        window.location.href = "/";
      }
     }
   }

   getCategoryProduct(){
     if(!this.flagCategoryProduct){
      if (localStorage.getItem('categoryProduct')) {
        this.categoryProducts = JSON.parse(localStorage.getItem('categoryProduct')) as CategoryProduct[]
        this.categoryProducts.forEach(categoryProduct =>{
          // categoryProduct.products = [];
          var products = [];
          if(categoryProduct.products[0].price != null){
            categoryProduct.products.forEach(product => {
              var discount = 0;
              var priceWithDiscount = "";
              if(product.discount != null){
                discount = product.discount;
                if(discount > 0){
                  priceWithDiscount = (product.price - discount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }
              }
              var productEdit: ProductEdit = {_id: product._id,
                title: product.title,
                priority: product.priority, 
                price: product.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
                discount: discount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
                priceWithDiscount: priceWithDiscount,
                count: 0,
                imageUrl: product.imageUrl,
                categoryProduct: null,
                ingredients: product.ingredients,
                available: product.available}
  
                products.push(productEdit);
            });
          }
          products.forEach(product =>{
             
            this.factor.factorDetails.forEach((factorDetail,index) =>{
                if(factorDetail.productId == product._id){
                  if(product.available != null && product.available == false){
                    // this.factor.factorDetails.splice(index, 1);
                  }else{
                    product.count = factorDetail.count;
                  }
                }
            })
          })
          var categoryProductEdit: CategoryProductEdit = {_id: categoryProduct._id, title: categoryProduct.title, priority: categoryProduct.priority, products: products}
          
          this.categoryProductsEdit.push(categoryProductEdit);
          this.calcBasket();
        })
        
        if(this.categoryProductsEdit[0].products.length == 0){
          this.getProducts(this.categoryProducts[0]._id,0,20);
        }

        var index = 0;
        this.categoryProductsEdit.forEach(categoryProduct => {
          var products = []
          if(index == 0){
            var size = 20;
            products = categoryProduct.products.slice(0, size);
          }
          index++;
          
          var categoryProductSample:CategoryProductEdit = {_id: categoryProduct._id,priority: categoryProduct.priority,products:[],title:categoryProduct.title}
          categoryProductSample.products = products;
          this.categoryProductsResult.push(categoryProductSample);
        });
        this.curentCategoryId = this.categoryProductsResult[0]._id;
        this.flagCategoryProduct = true;
        this.checkStop();
      }else{
        window.location.href = "/";
      }
     }
   }

  getProducts(categoryProduct_id:string, skip:number, limit: number){
    // this.startCountdown();
    this.isProductLoading = true;
    this.serviceProduct.getProductsBySkipLimit(categoryProduct_id,skip,limit).subscribe(products => {

      this.categoryProducts.forEach(categoryProduct => {
        if(categoryProduct._id == categoryProduct_id){
          if(skip == 0){
            categoryProduct.products = [];
          }
          products.forEach(product => {
            categoryProduct.products.push(product)
          });
        }
      });

      localStorage.setItem('categoryProduct', JSON.stringify(this.categoryProducts));

      this.categoryProductsEdit.forEach(categoryProduct => {
        if(categoryProduct._id == categoryProduct_id){

        products.forEach(product =>{
          var discount = 0;
          var priceWithDiscount = "";
          if(product.discount != null){
            discount = product.discount;
            if(discount > 0){
              priceWithDiscount = (product.price - discount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
          }
            var productEdit: ProductEdit = {_id: product._id,
                                            title: product.title,
                                            priority: product.priority, 
                                            price: product.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
                                            discount: discount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
                                            priceWithDiscount: priceWithDiscount,
                                            count: 0,
                                            imageUrl: product.imageUrl,
                                            categoryProduct: categoryProduct,
                                            ingredients: product.ingredients,
                                            available: product.available}
            var indexs=[]
            this.factor.factorDetails.forEach((factorDetail,index) =>{
                if(factorDetail.productId == product._id){
                  if(product.available != null && product.available == false){
                    // this.factor.factorDetails.splice(index, 1);
                    indexs.push(index);
                  }else{
                    productEdit.count = factorDetail.count;
                  }
                }
            })

            indexs.forEach(index => {
              this.factor.factorDetails.splice(index, 1);
            })
            localStorage.setItem('factor', JSON.stringify(this.factor));
            categoryProduct.products.push(productEdit);
          })


          this.categoryProductsResult.forEach(categoryProductResult => {
            if(categoryProductResult._id == categoryProduct._id){
              categoryProductResult.products = [];
              categoryProduct.products.forEach(product => {
                categoryProductResult.products.push(product);
              }); 
              if(this.lastSkipNumber < categoryProductResult.products.length ){
                this.flagEndScroll = false;
              }
            }
          });

         
        }

      });

      this.isProductLoading = false;
      
    },error => {
      this.isProductLoading = false;
          if(error.error != null){
           if(error.error.message != null){
             this.snackBar.open(error.error.message,
               "", { duration: 3000 ,panelClass: ['error-snackbar']});
           }else{
             this.snackBar.open('ارتباط با سرور برقرار نیست',
               "", { duration: 3000 ,panelClass: ['error-snackbar']});
           }
          }else{
            this.snackBar.open('اطلاعات وارد شده اشتباه است.',
                                "", { duration: 3000 ,panelClass: ['error-snackbar']});
          }
    },
    () => {
      
    })
  }

  ngOnInit() {
    this.imgHeight = this.scrHeight / 2.3
  }

  ngAfterViewInit(): void {
    var element = document.getElementById('tab-category')
    var tab = element.childNodes[1] as HTMLElement
    for(var i=1;i<tab.childNodes.length; i++){
      var tabContent = tab.childNodes[i].childNodes[0] as HTMLElement
      tabContent.style.height = (this.scrHeight - 309) + "px";
    }

 }

  // ngAfterViewChecked(){
  //   const elements = document.getElementsByClassName('mat-tab-body-content'); 

  //   for (let i = 0; i < elements.length; i++) {
      
  //       const element = elements[i] as HTMLElement;
  //       element.style.height = (this.scrHeight - 309) + "px";
  //       console.log(element);
  //   }
  // }

  checkStop(){
    if(this.flagSetting && this.flagCategoryProduct){
      this.stopCountDown();
   
    }
  }

  startCountdown(){
    this.counterDown = 10;
    this.isLoading = true;
    this.interval = setInterval(() => {
      this.counterDown--;
      
  
      if(this.counterDown < 0 ){
        
        this.isGetData = false;
        this.stopCountDown()
        this.snackBar.open('اتصال به اینترنت ضعیف یا قطع می باشد. لطفا دوباره سعی کنید.',
                              "", { duration: 3000 ,panelClass: ['error-snackbar']});
      };
    }, 1000);
  }

  stopCountDown(){
    this.isLoading = false;
    clearInterval(this.interval);
  }

  continueOrder(){
    var totalFactor = 0; 
    this.factor.factorDetails.forEach(factorDetail => {
      totalFactor += factorDetail.count * factorDetail.product.price;
    })

    if(!localStorage.getItem('currentUser')){
      window.location.href = "/#/marketkhoone/loginOrRegister/Order";
    }else{
      if(totalFactor == 0){
        this.snackBar.open('سبد خرید خالی است.',
        "", { duration: 3000 ,panelClass: ['danger-snackbar']});
      }else if (totalFactor < this.setting.minOrder){
        this.snackBar.open('سبد خرید کمتر از حد اقل سفارش می باشد.',
        "", { duration: 3000 ,panelClass: ['danger-snackbar']});
      }else{
        window.location.href = "/#/marketkhoone/address";
      }
    }
  }

  plus(productId){
    var productEdit: ProductEdit;
    this.categoryProductsEdit.forEach(categoryProduct => {
      categoryProduct.products.forEach(product => {
        if(product._id == productId){
          productEdit = product;
          product.count += 1;
        }
      })
    });

    var flagFind = false;
    this.factor.factorDetails.forEach(factorDetail => {
      factorDetail.newAdded = false;
      if(factorDetail.productId == productId){
        flagFind = true;
        factorDetail.count += 1;
        factorDetail.newAdded = true;
        localStorage.setItem('factor', JSON.stringify(this.factor));
      }
    })

    if(!flagFind){
      var categoryProduct: CategoryProduct;
      var product:Product = {_id: productEdit._id,
                             categoryProduct: categoryProduct,
                             imageUrl: productEdit.imageUrl,
                             ingredients: productEdit.ingredients,
                             price: Number( productEdit.price.replace(',','')),
                             discount: Number( productEdit.discount.replace(',','')),
                             priority: productEdit.priority,
                             title: productEdit.title,
                             available:productEdit.available}
      var factorDetail:EditFactorDetail = {count: 1,
                                           price: productEdit.price,
                                           discount: productEdit.discount,
                                           priceWithDiscount: productEdit.priceWithDiscount,
                                           product: product,
                                           productId: product._id,
                                           title: product.title,
                                           amount: productEdit.price,
                                           newAdded: true}

      this.factor.factorDetails.push(factorDetail);
      localStorage.setItem('factor', JSON.stringify(this.factor));
    }
    this.calcBasket();

  }

  minus(productId){
    var index = 0;
    var flagZero = false;
    this.categoryProductsEdit.forEach(categoryProduct => {
      categoryProduct.products.forEach(product => {
        if(product._id == productId){
          if(product.count > 0){
            product.count -= 1;

            this.factor.factorDetails.forEach((factorDetail, i) => {
              factorDetail.newAdded = false;
              if(factorDetail.productId == productId){
                factorDetail.count -= 1;
                factorDetail.newAdded = true;
                if(factorDetail.count == 1){
                  factorDetail.newAdded = false;
                }
                localStorage.setItem('factor', JSON.stringify(this.factor));
                if(factorDetail.count == 0){
                  flagZero = true;
                  index = i;
                }
              }
            })
          }
        }
      })
    });

    if (flagZero){
      this.factor.factorDetails.splice(index,1)
      localStorage.setItem('factor', JSON.stringify(this.factor));
    }

    this.calcBasket();
  }

  calcBasket(){
    var totalFactor = 0; 
    this.factor.factorDetails.forEach(factorDetail => {
      var discount = 0;
      if(factorDetail.product.discount != null){
        discount = factorDetail.product.discount;
      }
      totalFactor += factorDetail.count * (factorDetail.product.price - discount);
    })
 
    if(totalFactor > this.totalFactorNumber){
      var number = this.totalFactorNumber;
      var counter = Math.round((totalFactor - this.totalFactorNumber) / 19);
      this.totalFactorNumber = totalFactor;
      var interval = setInterval(() => { 
        number += counter;
        this.totalFactor = number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        if(number >= totalFactor){
          clearInterval(interval)
          this.totalFactor = totalFactor.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
          
        }
      }, 10);
    
    }else{
      var number = this.totalFactorNumber;
      var counter = Math.round((this.totalFactorNumber - totalFactor) / 19);
      this.totalFactorNumber = totalFactor;
      var interval = setInterval(() => { 
        number -= counter;
        this.totalFactor = number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        if(number <= totalFactor){
          clearInterval(interval);
          this.totalFactor = totalFactor.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }
      }, 10);
    }

    // this.totalFactor = totalFactor.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    // this.totalFactorNumber = totalFactor;
  }

  getAnimation(factorDetail){
    var element = document.getElementById("basket-" + factorDetail.product._id);
    if(factorDetail.count > 1){
      if(factorDetail.newAdded){
        if(element != null){
          element.classList.remove("basket-count-active");
          element.offsetWidth;
          element.classList.add("basket-count-active");
          factorDetail.newAdded = false;
        }
      }
    }else{
      if(element != null){
        element.classList.remove("basket-count-active");
      }
    }
    return true;
  }

  openIngredient(product){
    this.isIngredient = true;

    this.product = product as ProductEdit;
    var element = document.getElementById("ingredient-modal");
    element.classList.remove("open-modal");
    element.classList.remove("close-modal");
    element.offsetWidth;
    element.classList.add("open-modal");
    this.ingeradientHeight = 100;
  }

  closeIngredient(){
    var element = document.getElementById("ingredient-modal");
    element.classList.remove("open-modal");
    element.classList.remove("close-modal");
    element.offsetWidth;
    element.classList.add("close-modal");

    var interval = setInterval(() =>{
      this.isIngredient = false;
      clearInterval(interval);
      this.ingeradientHeight = 0;
    },1000)
  }

  retryGetData(){
    this.isGetData = true
    this.startCountdown()
    this.getSetting();
    this.getCategoryProduct();
  }

  // setHeight(event){
  //   var element = event.tab._implicitContent._projectedViews[0].nodes[1].renderElement.parentElement as HTMLElement
  //   element.style.height = (this.scrHeight - 309) + "px";
  // }
  search(){
    window.location.href = "/#/marketkhoone/search";
  }

  scrollHandler(event){

    var element = document.getElementById("searchFab");

    if(event.srcElement.scrollTop > this.lastScroll){
      element.classList.remove("fade-in-search-fab");
      element.classList.add("fade-out-search-fab");
    }else{
      element.classList.remove("fade-out-search-fab");  
      element.classList.add("fade-in-search-fab");
    }

    this.lastScroll = event.srcElement.scrollTop;

    if( event.srcElement.scrollTop >= (event.srcElement.scrollHeight - event.srcElement.offsetHeight - 2))
    {
      if(!this.flagEndScroll){
        this.flagEndScroll = true;
        this.categoryProductsResult.forEach(categoryProductResult => {
          if(categoryProductResult._id == this.curentCategoryId){
            this.lastSkipNumber = categoryProductResult.products.length;

            this.categoryProductsEdit.forEach(categoryProduct => {
              if(categoryProduct._id == this.curentCategoryId){
                if(categoryProductResult.products.length < categoryProduct.products.length){
                  var size = categoryProductResult.products.length + 20;
                  var products = categoryProduct.products.slice(0, size);
                  categoryProductResult.products = products;
                  this.flagEndScroll = false;
                }else{
                  if(this.lastSkipNumber%20 == 0){
                    this.getProducts(categoryProductResult._id,this.lastSkipNumber,20);
                  }
                }
              }
            });

          }
        });
      }
    }
  }

  tabClick(event){

    this.lastSkipNumber = 0;
    this.flagEndScroll = false;
    // this.categoryProductsResult = [];
    this.categoryProductsResult.forEach(categoryProductResult => {
      
      if(categoryProductResult.title == event.tab.textLabel){
        this.curentCategoryId = categoryProductResult._id;
        this.categoryProductsEdit.forEach(categoryProduct => {
          if(categoryProduct._id == this.curentCategoryId){
            if(categoryProduct.products.length == 0){
              this.getProducts(categoryProduct._id,0,20);
            }else{
              var size = 20;
              var products = categoryProduct.products.slice(0, size);
              categoryProductResult.products = products;
              this.lastSkipNumber = 20;
            }
            
          }
        });
      }else{
        categoryProductResult.products = [];
      }
    });


  }

}

