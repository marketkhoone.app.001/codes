import { result, notFound, error } from 'express-easy-helper';
import { emit } from '../sockets/factor.socket';
import { addAudit } from '../controllers/audit.controller';
import Factor from '../models/factor.model';
import FactorDetail from '../models/factorDetail.model';
import User from '../models/user.model';
import DiscountCode from '../models/discountCode.model';
import Product from '../models/product.model';
import Address from '../models/address.model';
import FreightArea from '../models/freightArea.model';
import Setting from '../models/setting.model';


var persianDate = require('persian-date');
var geolib = require('geolib')
var dateFormat = require('dateformat');

var Kavenegar = require('kavenegar');
var api = Kavenegar.KavenegarApi({apikey: '44616F316A6A623863456F374A687A30394A61443038516571456864627671544343497450702F79454C553D'});

var to = '';
const from = '10008445';
var text = '';
// List Factor's
export function listByUser(req, res) {

  let user = req.user;
  delete user.session.id;

  User.findOne({_id: user._id})
  .populate({path:'factors',populate: {path:'factorDetails', populate: {path: 'product' , model: 'Product'}}})
  .populate('product')
  .populate({path:'factors',populate: {path:'discountCode', model: 'DiscountCode'}})
  .populate('discountCode')
  .populate({path:'factors',populate: {path:'address', model: 'Address'}})
  .populate('address')
  .then(user => {
    // var factors = [];
    // user.factors.forEach(function(factor) {
    //   if (factor.complete == true){
    //     factors.push(factor)
    //   }
    // });
    return user.factors
  })
  .then(notFound(res))
  .then(result(res))
  .catch(error(res));
}

// List Factor's
export function list(req, res) {

  return Factor.find()
  .populate({path:'factorDetails', populate: {path: 'product' , model: 'Product'}})
  .populate('product')
  .populate('discountCode')
  .populate('address')
  .populate('user')
  .sort({"product" : 1}).exec()
    .then(notFound(res))
    .then(result(res))
    .catch(error(res));
}

export function chartDashboard(req, res) {


  Factor.find({complete: true},{dateNumber: 1}).select("+dateNumber").then(factors => {
    if(factors.length == 0){
      var chartDashboard = {
        year: [],
        dataForWeekPeriod: [0,0,0,0,0,0,0],
        dataForMonthPeriod: [0,0,0,0,0,0,0,0,0,0,0,0],
        dataForYearPeriod: [0],
        lastYear: 0,
        lastMonth: 0,
        lastWeek: 0,
        today: 0
      }  
  
      return res.status(200).send({ succes: true, chartDashboard: chartDashboard })
    }else{
      var now = new persianDate()
      now.toLocale('fa');
      var formattedWeek = now.format("dddd");
      var formattedMonth = now.format("MMMM");
      var rangeWeeks = persianDate.rangeName().weekdays
      var rangeMonths = persianDate.rangeName().months;
      now.toLocale('en');
      var formatedDateNumber = now.format("YYYYMMDD");
      var currentDateNumber = Number(formatedDateNumber)
      var lastYear = 0;
      var lastMonth = 0;
      var lastWeek = 0;
      var today = 0;
      var year = []
      var firstYear = Number(factors[0].dateNumber.toString().substr(0,4))
      var nowYear = Number(factors[factors.length - 1].dateNumber.toString().substr(0,4))

      var dataForYearPeriod = []
      for (var i = firstYear; i <= nowYear; i++) { 
        year.push(i.toString());
        dataForYearPeriod.push(getCount(factors,i,currentDateNumber,false,false))
      }
      // ---------------------------------------------
      
      var indexWeek = rangeWeeks.indexOf(formattedWeek);

      var dataForWeekPeriod = [0,0,0,0,0,0,0]

      var valueDateNumber = currentDateNumber
      for (var i = indexWeek; i >= 0; i--) { 
        dataForWeekPeriod[i] = getCount(factors,valueDateNumber--,currentDateNumber,true,false)
      }
      // ---------------------------------------------
      
      var indexMonth = rangeMonths.indexOf(formattedMonth);

      var dataForMonthPeriod= [0,0,0,0,0,0,0,0,0,0,0,0]

      for (var i = indexMonth; i >= 0; i--) { 
        dataForMonthPeriod[i] = getCount(factors,i + 1,currentDateNumber,false,true)
      }
      // ---------------------------------------------

      today = getCount(factors,currentDateNumber,currentDateNumber,true,false)
      // ---------------------------------------------

      for (var i = 0; i <= 6; i++ ){ 
        lastWeek += getCount(factors,currentDateNumber - indexWeek - i - 1 ,currentDateNumber,true,false)
      }
      // ---------------------------------------------

      lastMonth = getCount(factors,indexMonth,currentDateNumber,false,true)
      // ---------------------------------------------

      lastYear = getCount(factors,Number(currentDateNumber.toString().substr(0,4)) - 1,currentDateNumber,false,false)

      var chartDashboard = {
        year: year,
        dataForWeekPeriod: dataForWeekPeriod,
        dataForMonthPeriod: dataForMonthPeriod,
        dataForYearPeriod: dataForYearPeriod,
        lastYear: lastYear,
        lastMonth: lastMonth,
        lastWeek: lastWeek,
        today: today
      }  
  
      return res.status(200).send({ succes: true, chartDashboard: chartDashboard })
    }
   
  })
}

function getCount(factors,value,currentDateNumber,isWeek,isMonth){
  var filtered = []
  if (isWeek){
    filtered = factors.filter(function(factor) {
      return (factor.dateNumber == value);
    })
  }else if(isMonth){
    filtered = factors.filter(function(factor) {
      var nowYear = currentDateNumber.toString().substr(0,4)
      var year = Number(factor.dateNumber.toString().substr(0,4))
      var month = Number(factor.dateNumber.toString().substr(4,2))
      return (nowYear == year && month == value);
    })
  }else{
    filtered = factors.filter(function(factor) {
      var year = Number(factor.dateNumber.toString().substr(0,4))

      return (year == value);
    })
  }
  
  return filtered.length
}

function getCountBetween(value){
  var endValue = new Date();

  var filtered = markers.filter(function(marker) {
  var markerDate = new Date(marker.date);
  return (markerDate > startValue && markerDate < endValue);
  })
}

export function listOpen(req, res) {

  return Factor.find({complete: false})
  .populate({path:'factorDetails', populate: {path: 'product' , model: 'Product'}})
  .populate('product')
  .populate('discountCode')
  .populate('address')
  .populate('user')
  .sort({"number" : 1}).exec()
    .then(notFound(res))
    .then(result(res))
    .catch(error(res));
}

export function listComplete(req, res) {

  return Factor.find({complete: true})
  .populate({path:'factorDetails', populate: {path: 'product' , model: 'Product'}})
  .populate('product')
  .populate('discountCode')
  .populate('address')
  .populate('user')
  .sort({"number" : -1}).exec()
    .then(notFound(res))
    .then(result(res))
    .catch(error(res));
}

// Create a Factor
export function create(req, res) {

  let user = req.user;
  delete user.session.id;

  User.findOne({_id: user._id}).then(user => {

    const newFactor = new Factor();

    var now = new persianDate();
    now.toLocale('en');
    var formatted = now.format('YYMMDDHHmmss');
    newFactor.number = formatted + makeConfirmCode()
    
    var totalAmount = 0
    var itemsProcessed = 0;

    
    req.body.factorDetails.forEach((factorDetail, index, array)=>{
      Product.findById(factorDetail.productId).then(product =>{
        const newFactorDetail = new FactorDetail({
          title: product.title,
          price: product.price,
          discount: product.discount,
          count: factorDetail.count,
          product: product,
        })
        newFactorDetail.save()
        newFactor.factorDetails.push(newFactorDetail)
        if (product.discount == null){
          product.discount = 0;
        }
        totalAmount += ((product.price - product.discount) * factorDetail.count)
        itemsProcessed++;

        if (itemsProcessed === (array.length)) {
          newFactor.totalAmount = totalAmount

          Address.findById(req.body.addressId).then(address=>{

            FreightArea.find({}).sort({"priority" : 1}).then(freightAreas =>{
              var find = false
              freightAreas.forEach(freightArea => {
                var polygon = []
                var split = freightArea.cordinates.substr(0, freightArea.cordinates.length - 1).substr(1).split("], [")
        
                split.forEach(cordinate=>{
                  var splitCordinate = cordinate.split(",")
                  var point = {latitude: Number(splitCordinate[1]), longitude: Number(splitCordinate[0])};
                  polygon.push(point) 
                })
        
                if (geolib.isPointInside({latitude: Number(address.latitude), longitude:  Number(address.longitude)},polygon) && !find){
                  find = true
                  newFactor.freight = freightArea.freight
                  newFactor.locationBody = address.location
                  newFactor.locationLat = address.latitude
                  newFactor.locationLng = address.longitude
                  newFactor.address = address

                  var isValidDiscountId = true;
                  if (req.body.discountCodeId == ''){
                    req.body.discountCodeId = "000000000000000000000000"
                    isValidDiscountId = false;
                  }
                  DiscountCode.findById(req.body.discountCodeId).then(discountCode => {
                    if (!isValidDiscountId){
                      discountCode = false;
                    }
                    if (discountCode){
                      if (totalAmount > discountCode.maxOrder){
                        newFactor.discount = ((discountCode.maxOrder * discountCode.discount) / 100)
                        newFactor.discountCodeBody = discountCode.code
                        newFactor.discountCode = discountCode
                      }else{
                        newFactor.discount = ((totalAmount * discountCode.discount) / 100)
                        newFactor.discountCodeBody = discountCode.code
                        newFactor.discountCode = discountCode
                      }
                    }else{
                      newFactor.discount = 0
                    }

                    Setting.findOne().then(setting=>{

                      var nowDate = new persianDate()
                      nowDate.toLocale('en');
                      var hour = Number(nowDate.format("H"));
                      var flagIsStoreOpen = false;
                  
                      console.log("hour: " + hour)
                  
                      if((setting.fromTime >= setting.toTime)){
                        if(hour >= setting.fromTime && hour > setting.toTime){
                          flagIsStoreOpen = true
                        }else if(hour < setting.fromTime && hour < setting.toTime){
                          flagIsStoreOpen = true
                        }else{
                          flagIsStoreOpen = false
                        }
                      }else{
                        if(hour >= setting.fromTime && hour < setting.toTime){
                          flagIsStoreOpen = true
                        }else{
                          flagIsStoreOpen= false
                        }
                      }

                      if(flagIsStoreOpen){
                        newFactor.packing = setting.packing
                        newFactor.tax = Math.round(((((newFactor.totalAmount + newFactor.packing + newFactor.freight) - newFactor.discount) * setting.tax) / 100))
                        newFactor.finalAmount = (newFactor.totalAmount + newFactor.packing + newFactor.freight + newFactor.tax) - newFactor.discount
                        var now = new persianDate()
                        now.toLocale('en');
                        var formatted = now.format("YYYY-MM-DD'T'HH:mm:ss");
                        newFactor.date = formatted
                        var formattedNumber = now.format("YYYYMMDD");
                        newFactor.dateNumber = Number(formattedNumber)
                        newFactor.paymentOnline = req.body.paymentOnline
                        newFactor.complete = false
                        newFactor.userMobileNumber = user.mobileNumber
                        newFactor.userName = user.name
                        newFactor.userLastname = user.lastname
                        newFactor.user = user._id
  
                        user.factors.push(newFactor)
                        user.save().then(savedUser=>{
  
                          addAudit(user.name + " " + user.lastname,'Factor','POST',newFactor._id,newFactor,'null')
                          emit('factor','add')
                          return newFactor.save()
                          .then(result(res, 201))
                          .catch(error(res));
                        })
                      }else{
                        return res.status(500).send({ succes: false, message: 'ساعت کار فروشگاه از ساعت ' + setting.fromTime + ' الی ' + setting.toTime + ' میباشد.'})
                      }
                    })
                  })
                }
              })
            }) 
          })
        }
      })
    })
  })
}

// read a Factor
export function read(req, res) {

  let user = req.user;
  delete user.session.id;

  User.findOne({_id: user._id})
  .populate({path:'factors',populate: {path:'factorDetails', populate: {path: 'product' , model: 'Product'}}})
  .populate('product')
  .populate({path:'factors',populate: {path:'discountCode', model: 'DiscountCode'}})
  .populate('discountCode')
  .populate({path:'factors',populate: {path:'address', model: 'Address'}})
  .populate('address')
  .then(user => {
    let find = false
    user.factors.forEach(factor => {
      if (factor._id == req.swagger.params.id.value){
        find = true

        return Factor.findById(req.swagger.params.id.value).exec()
        .then(notFound(res))
        .then(result(res))
        .catch(error(res));
      }
    })
    if (!find){
      return res.status(500).send({ succes: false, message: 'اطلاعات کاربر اشتباه است.' })
    }  })
}

// Complet a Factor
export function completeFactor(req, res) {

  let user = req.user;
  delete user.session.id;

  return Factor.findById(req.swagger.params.id.value).populate('user').then(factor=>{

    var lastData = new Factor({
      number: factor.number,
      tax: factor.tax,
      freight: factor.freight,
      packing: factor.packing,
      discount: factor.discount,
      totalAmount: factor.totalAmount,
      finalAmount: factor.finalAmount,
      date: factor.date,
      paymentOnline: factor.paymentOnline,
      locationBody: factor.locationBody,
      locationLat: factor.locationLat,
      locationLng: factor.locationLng,
      discountCodeBody: factor.discountCodeBody,
      complete: factor.complete,
      discountCode: factor.discountCode,
      address: factor.address,
      userMobileNumber: factor.userMobileNumber,
      userName: factor.userName,
      userLastname: factor.userLastname,
      user: factor.user._id,
      factorDetails: factor.factorDetails,
    })

    factor.complete = true;
    var now = new persianDate();
    now.toLocale('en');
    var formatted = now.format("YYYY-MM-DD'T'HH:mm:ss");
    factor.completeDate = formatted;

    emit('factor','add')
    addAudit(user.name + " " + user.lastname,'Factor','COMPLETE',factor._id,factor,lastData);

    return factor.save()

    .then(factor=>{
      Setting.findOne().then(setting=>{
        if (setting.sms - 1 > 0){
          to = factor.user.mobileNumber
          api.VerifyLookup({
            receptor: to,
            token10: "فست فود بوفالو",
            token: factor.number,
            template: "completefactor"
          }, function(response, status, cost) {
              console.log(response);
              console.log(status);
          });
          var lastData1 = new Setting({
            sms: setting.sms,
          });
          setting.sms = setting.sms - 1

          addAudit(user.name + " " + user.lastname,'Factor','Send SMS',factor._id,setting,lastData1)
      
          setting.save()

          return res.status(201).json(user);
        }else{
          return res.status(201).json(user);
        }
      });
      
    })
    .catch(error(res));
  })
}

// Update a Factor
export function update(req, res) {
  let user = req.user;
  delete user.session.id;

  return Factor.findById(req.swagger.params.id.value)
  .populate('factorDetails')
  .populate('user')
  .then(factor => {
    var lastData = new Factor({
      number: factor.number,
      tax: factor.tax,
      freight: factor.freight,
      packing: factor.packing,
      discount: factor.discount,
      totalAmount: factor.totalAmount,
      finalAmount: factor.finalAmount,
      date: factor.date,
      paymentOnline: factor.paymentOnline,
      locationBody: factor.locationBody,
      locationLat: factor.locationLat,
      locationLng: factor.locationLng,
      discountCodeBody: factor.discountCodeBody,
      complete: factor.complete,
      discountCode: factor.discountCode,
      address: factor.address,
      userMobileNumber: factor.userMobileNumber,
      userName: factor.userName,
      userLastname: factor.userLastname,
      user: factor.user,
      factorDetails: factor.factorDetails,
    })
    var totalAmount = 0;
    var itemsProcessed = 0;
    console.log("find factor")
    if(factor.factorDetails.length == 0 || req.body.factorDetails.length == 0){
      return res.status(500).send({ succes: false, message: 'اقلام فاکتور را انتخاب کنید' })
    }
    factor.factorDetails.forEach((oldFactorDetail, index, array)=>{
      console.log("factorDetails " + index)
      FactorDetail.findByIdAndRemove(oldFactorDetail._id).exec()
      itemsProcessed++;

      if (itemsProcessed === (array.length)) {
        itemsProcessed = 0
        factor.factorDetails = []

        req.body.factorDetails.forEach((factorDetail, index, array)=>{
          Product.findById(factorDetail.productId).then(product =>{
            const newFactorDetail = new FactorDetail({
              title: product.title,
              price: product.price,
              discount: product.discount,
              count: factorDetail.count,
              product: product,
            })
            newFactorDetail.save()
            factor.factorDetails.push(newFactorDetail)
            totalAmount += ((product.price - product.discount) * factorDetail.count)
            itemsProcessed++;

            if (itemsProcessed === (array.length)) {
              factor.totalAmount = totalAmount
    
              Address.findById(req.body.addressId).then(address=>{
                console.log("find address")
                FreightArea.find({}).sort({"priority" : 1}).then(freightAreas =>{
                  var find = false
                  freightAreas.forEach(freightArea => {
                    var polygon = []
                    var split = freightArea.cordinates.substr(0, freightArea.cordinates.length - 1).substr(1).split("], [")
            
                    split.forEach(cordinate=>{
                      var splitCordinate = cordinate.split(",")
                      var point = {latitude: Number(splitCordinate[1]), longitude: Number(splitCordinate[0])};
                      polygon.push(point) 
                    })
            
                    if (geolib.isPointInside({latitude: Number(address.latitude), longitude:  Number(address.longitude)},polygon) && !find){
                      find = true
                      factor.freight = freightArea.freight
                      factor.locationBody = address.location
                      factor.locationLat= address.latitude
                      factor.locationLng = address.longitude
                      factor.address = address
    
                      var isValidDiscountId = true;
                      if (req.body.discountCodeId == ''){
                        req.body.discountCodeId = "000000000000000000000000"
                        isValidDiscountId = false;
                      }
                      DiscountCode.findById(req.body.discountCodeId).then(discountCode => {
                        console.log("find discount")
                        if (!isValidDiscountId){
                          discountCode = false;
                        }
                        if (discountCode){
                          if (totalAmount > discountCode.maxOrder){
                            factor.discount = ((discountCode.maxOrder * discountCode.discount) / 100)
                            factor.discountCodeBody = discountCode.code
                            factor.discountCode = discountCode
                          }else{
                            factor.discount = ((totalAmount * discountCode.discount) / 100)
                            factor.discountCodeBody = discountCode.code
                            factor.discountCode = discountCode
                          }
                        }else{
                          factor.discount = 0
                        }
    
                        Setting.findOne().then(setting=>{
                          console.log("find setting")
                          factor.packing = setting.packing
                          factor.tax = Math.round(((((factor.totalAmount + factor.packing + factor.freight) - factor.discount) * setting.tax) / 100))
                          factor.finalAmount = (factor.totalAmount + factor.packing + factor.freight + factor.tax) - factor.discount
                          factor.paymentOnline = req.body.paymentOnline
    
                          addAudit(user.name + " " + user.lastname,'Factor','PUT',factor._id,factor,lastData)
                          console.log("end")
                          return factor.save()
                            .then(result(res, 201))
                            .catch(error(res));
    
                        })
                      })
                    }
                  })
                }) 
              })
            }
          })
        })
      }
    })
  })
}

// Destroy a Factor
export function destroy(req, res) {

   let user = req.user;
   delete user.session.id;

  // User.findOne({_id: user._id})
  // .populate({path:'factors',populate: {path:'factorDetails', model: 'FactorDetail'}})
  // .populate('factorDetails')
  // .then(user => {
  //   console.log("find user")
  //   let find = false
  //   console.log(user)

  //   user.factors.forEach(factor => {
  //     console.log(req.swagger.params.id.value)
  //     if (factor._id == req.swagger.params.id.value){
  //       find = true
  //       console.log("find factor")

        Factor.findOne({_id: req.swagger.params.id.value})
        .populate('factorDetails')
        .then(factor => {

          User.findOneAndUpdate({factors: req.swagger.params.id.value}, {$pull: {factors: req.swagger.params.id.value}}, (err, data) => {

            if (err){
              console.log(err)
            }
            if (factor.factorDetails.length > 0){
              factor.factorDetails.forEach(factorDetail => {
                factorDetail.remove()
              })
            }

            addAudit(user.name + " " + user.lastname,'Factor','DELETE',factor._id,'null',factor)

            return factor.remove()
            .then(result(res))
            .catch(error(res));
          })
          
        })
  //     }
  //   })

    
  // })
}

// Emit animation with socket!
export function animation(req, res) {
  try {
    emit('animation', req.swagger.params.action.value);
    return result(res, 'Socket emitted!');
  } catch (err) {
    return error(res, 'No client with event...');
  }
}

function makeConfirmCode() {

  var text = "";

  var possible = "0123456789";
  for (var i = 0; i < 3; i++){
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}