import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { NotificationsService} from 'angular2-notifications';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { CategoryProduct } from '../../models/category-product';
import { Product } from '../../models/product';
import { Ingredient } from '../../models/ingredient';
import { EditFactorDetail } from '../../models/factor-detail';
import { EditFactor } from '../../models/factor';
import { Globals } from '../../globals';

declare function startScript(): any;

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit, AfterViewInit  {
  titleProduct: string;
  titleCategoryProduct: string;
  quantity: string;
  id: string;
  categoryProduct: CategoryProduct = {_id: '', priority: 0, title: '', products: []}
  product: Product = {_id: '', price: 0, discount: 0, imageUrl: '', title: '', priority: 0, categoryProduct: this.categoryProduct, ingredients: [],available: true}
  ingredients: String[] = []

  constructor(private titleService: Title, private meta: Meta, private route: ActivatedRoute, private service: ProductService, private notification: NotificationsService, public globals: Globals){

    this.id = this.route.snapshot.params['id'];
    this.titleCategoryProduct = this.route.snapshot.params['categoryTitle'];
    this.titleProduct = this.route.snapshot.params['title'];

    this.service.getProduct(this.id).subscribe(product => {
      this.product = product
      this.product.ingredients.forEach(ingredient =>{
        this.ingredients.push(ingredient.title)
      })
      this.categoryProduct = this.product.categoryProduct

      if ((encodeURI(this.titleCategoryProduct) != encodeURI(this.categoryProduct.title.split(' ').join('-'))) || (encodeURI(this.titleProduct) != encodeURI(this.product.title.split(' ').join('-')))){
        window.location.href = "/product/"+ this.product._id + "/" + this.categoryProduct.title.split(' ').join('-') + "/" + this.product.title.split(' ').join('-');
      }
      
      this.seo();
    },error => {
      if(error.error != null){
         if(error.error.message != null){
            this.notification.error(null,error.error.message)
          }else{
            this.notification.error(null,'ارتباط با سرور برقرار نیست.')
          }
      }else{
        this.notification.error(null,'اطلاعات وارد شده اشتباه است.')
      }
    },
    () => {
      
    })

    if (localStorage.getItem('isAddedNew')) {
      var isNew = localStorage.getItem('isAddedNew')
      if(isNew == 'true'){
        this.notification.success(null,'کالا ثبت شد ( <a href="/cart">مشاهده سبد خرید</a> )')
        localStorage.setItem('isAddedNew','false')
      }
    }
   }

   seo(){
    const uri = this.globals.siteUrl + "/product/" + this.product._id + "/" + this.categoryProduct.title.split(' ').join('-') + "/" + this.product.title.split(' ').join('-')
    var url = encodeURI(uri);
    this.meta.addTag({ name: 'og:url', content: url})

    var keywords = ""; 
    this.ingredients.forEach(ingredient => {
      keywords = keywords + (ingredient + " ، ")
    });

    var description = "فروش آنلاین " + this.product.title + " در گروه کالای " + this.categoryProduct.title + " در " + this.globals.storeName + " شامل " + keywords + " می باشد"
    this.meta.addTag({ name: 'og:description', content: description })
    this.meta.addTag({ name: 'description', content: description })

    keywords = keywords +  this.globals.storeName + " ، "
    keywords = keywords + (this.categoryProduct.title + " ، ")
    keywords = keywords + this.product.title

    this.meta.addTag({ name: 'keywords', content: keywords })

    var image = this.product.imageUrl
    this.meta.addTag({ name: 'og:image', content: image })

    var title =  this.globals.storeName + " | " + this.categoryProduct.title + " | " + this.product.title 
    this.meta.addTag({ name: 'og:title', content: title })

    this.titleService.setTitle(title);
  }

  ngOnInit() {
   
  }

  ngAfterViewInit() {
    startScript();
  }

  addToCart() {
    if (!isNaN(Number(this.quantity))){
      if (Number(this.quantity) > 0){
        var factor = JSON.parse(localStorage.getItem('factor')) as EditFactor
        var findElement = false;
        factor.factorDetails.forEach(element => {
         
          if (element.productId == this.id){
            var discount = 0;
            var priceWithDiscount = "";
            if(this.product.discount != null){
              discount = this.product.discount;
              if(this.product.discount > 0){
                priceWithDiscount = (this.product.price - discount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              }
            }
            element.count =  Number(this.quantity)
            element.price = this.product.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            element.discount = discount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            element.priceWithDiscount = priceWithDiscount
            element.amount = (this.product.price * Number(this.quantity)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            findElement = true;
          }
        });
    
        if (!findElement){
          var iDiscount = 0;
          var iPriceWithDiscount = "";
          if(this.product.discount != null){
            iDiscount = this.product.discount;
            if(this.product.discount > 0){
              iPriceWithDiscount = (this.product.price - iDiscount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
          }
          var price = this.product.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          var discount = iDiscount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          var priceWithDiscount = iPriceWithDiscount;
          var amount = ((this.product.price - iDiscount) * Number(this.quantity)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          var factorDetail: EditFactorDetail = {count: Number(this.quantity), product: this.product, productId: this.id, title: this.product.title, price: price, discount: discount, priceWithDiscount: priceWithDiscount, amount: amount}
          factor.factorDetails.push(factorDetail)
        }

        localStorage.setItem('factor', JSON.stringify(factor));
        localStorage.setItem('isAddedNew','true')
        window.location.reload();
      }
    }
  }

}
