import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { NotificationsService} from 'angular2-notifications';
import { ProductService } from '../../../@core/data/product.service';
import { AuthenticationService } from '../../../@core/data/authentication.service';
import { Product } from '../../../@core/models/product'
import { CategoryProductDropdownEditorComponent } from '../../component/category-product-dropdown-editor.component';
import { UploadImageEditorComponent } from '../../component/upload-image-editor.component';
import { Helper } from '../../../helpers/helper';
import { FileUploadService } from '../../../@core/data/file-upload.service';
import { AvailableButtonViewComponent } from '../../component/available-button-editor.component';

@Component({
  selector: 'ngx-product',
  templateUrl: './product.component.html',
  styles: [`a
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})

export class ProductComponent {

  

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    pager: {
      display: true,
      perPage: 10,
    },
    actions: {
      columnTitle: 'عملیات ها'
    },
    columns: {
        categoryProduct: {
            title: 'دسته بندی',
            type: 'html',
            filter: false,
            width: '15%',
            valuePrepareFunction: (cell, row) => { return row.categoryProduct.title},
            editor: {
              type: 'custom',
              component: CategoryProductDropdownEditorComponent,
            },
          },
        priority: {
            title: 'ترتیب نمایش',
            type: 'number',
            sort: true, 
            sortDirection: 'asc',
            filter: false,
            width: '10%'
        },
        title: {
            title: 'نام',
            type: 'string',
            filter: false,
            width: '15%'
        },
        price: {
            title: 'قیمت',
            type: 'string',
            filter: false,
            width: '10%'
        },
        discount: {
          title: 'تخفیف',
          type: 'string',
          filter: false,
          width: '10%'
        },
        imageUrl: {
            title: 'تصویر',
            type: 'html',
            filter: false,
            width: '15%',
            valuePrepareFunction: (cell, row) => { return `<a href="${row.imageUrl}"><img class= "img-table" src="${row.imageUrl}" /></a>`},
            editor: {
              type: 'custom',
              component: UploadImageEditorComponent,
            },
        },
        button: {
          title: 'موجودی',
          type: 'custom',
          filter: false,
          editable: false,
          width: '15%',
          renderComponent: AvailableButtonViewComponent,
          onComponentInitFunction: (instance) => {
            instance.updateResult.subscribe(updatedUserData => {
              this.refresh()
            });
          },
        },
    },
  };

  

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: ProductService, private fileUploadServise: FileUploadService, private authentication: AuthenticationService, private notification: NotificationsService) {
    this.service.getProducts().subscribe(products => {
      this.source.load(products);
    },error => {
      this.notification.error(null,'مشکلی در دریافت اطلاعات وجود دارد.')
      if(error.status == 403 || error.status == 401){
        this.authentication.logout()
      }
    },
    () => {
      
    })
  }

  onDeleteConfirm(event) {

    if (window.confirm('برای حذف ' + event.data['title'] + ' مطمئن هستید؟')) {

      var array = event.data['imageUrl'].toString().split("/")
      var fileName = array[array.length - 1]
      if(fileName != ""){
        this.fileUploadServise.removeImage(fileName).subscribe(result =>{
          this.notification.success(null,'عکس با موفقیت حذف شد.')
        },error => {
          this.notification.error(null,'مشکلی در ثبت اطلاعات وجود دارد.')
        },
        () => {
          
        })
      }
      
      this.service.removeProduct(event.data as Product).subscribe(() =>{
        event.confirm.resolve();
        this.notification.success(null,'اطلاعات با موفقیت ثبت شد.')
      },error => {
        this.notification.error(null,'مشکلی در ثبت اطلاعات وجود دارد.')
        if(error.status == 403 || error.status == 401){
          this.authentication.logout()
        }
      },
      () => {
        
      })
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event) {
    if((Number(Helper.fixNumbers(event.newData.price.toString())) <= Number(Helper.fixNumbers(event.newData.discount.toString()))) ||  ( Number(Helper.fixNumbers(event.newData.discount.toString())) < 0) || (event.newData.discount.toString() == "")){
      this.notification.error(null,'مقدار فیلد تخفیف درست نیست.')
    }else{
      const editedProduct : Product = {_id: event.newData._id,
        title: Helper.fixNumbers(event.newData.title.toString()),
        priority: Number(Helper.fixNumbers(event.newData.priority.toString())),
        categoryProduct: event.newData.categoryProduct,
        price: Number(Helper.fixNumbers(event.newData.price.toString())),
        discount: Number(Helper.fixNumbers(event.newData.discount.toString())),
        imageUrl: event.newData.imageUrl,
        ingredients: event.newData.ingredients,
        available: event.newData.available
      }

      this.service.updateProduct(editedProduct).subscribe(product =>{
      event.confirm.resolve(product);
      this.notification.success(null,'اطلاعات با موفقیت ثبت شد.')
      },error => {
      this.notification.error(null,'مشکلی در ثبت اطلاعات وجود دارد.')
      if(error.status == 403 || error.status == 401){
      this.authentication.logout()
      }
      },
      () => {

      })
    }
    
  }

  onCreateConfirm(event) {
    if((Number(Helper.fixNumbers(event.newData.price.toString())) <= Number(Helper.fixNumbers(event.newData.discount.toString()))) ||  ( Number(Helper.fixNumbers(event.newData.discount.toString())) < 0) || (event.newData.discount.toString() == "")){
      this.notification.error(null,'مقدار فیلد تخفیف درست نیست.')
    }else{
      this.service.saveProduct({title: Helper.fixNumbers(event.newData.title.toString()),
        priority: Number(Helper.fixNumbers(event.newData.priority.toString())),
        categoryProduct: event.newData.categoryProduct,
        price: Number(Helper.fixNumbers(event.newData.price.toString())),
        discount: Number(Helper.fixNumbers(event.newData.discount.toString())),
        imageUrl: event.newData.imageUrl} as Product).subscribe(
                                                                  product =>{

                                                                  event.confirm.resolve(product);
                                                                  this.notification.success(null,'اطلاعات با موفقیت ثبت شد.')
                                                                  },error => {
                                                                  this.notification.error(null,'مشکلی در ثبت اطلاعات وجود دارد.')
                                                                  if(error.status == 403 || error.status == 401){
                                                                  this.authentication.logout()
                                                                  }
                                                                  },
                                                                  () => {

                                                                  })
    }
    
  }

  refresh(){
    this.service.getProducts().subscribe(products => {
      this.source.load(products);
    },error => {
      this.notification.error(null,'مشکلی در دریافت اطلاعات وجود دارد.')
      if(error.status == 403 || error.status == 401){
        this.authentication.logout()
      }
    },
    () => {
      
    })
  }

  onSearch(query: string = '') {
    if (query == ''){
      this.source.setFilter([], true);
    }else{
      this.source.setFilter([
        {
          field: 'priority',
          search: query,
        },
        {
          field: 'title',
          search: query,
        },
        {
          field: 'price',
          search: query,
        },
        {
          field: 'discount',
          search: query,
        },
        {
          field: 'categoryProduct',
          search: query,
          filter: (cell?: any, search?: string) => {
            
            if (cell.title.indexOf(search) > -1){
              return true;
            }else{
              return false;
            }
        },
        }
      ], false);
    }
  }
}
