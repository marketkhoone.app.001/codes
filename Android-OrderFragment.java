package com.marketkhoone.foodmarket.buffalo.ui.main.order;


import android.animation.ValueAnimator;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
//import android.support.v4.app.Fragment;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.marketkhoone.foodmarket.buffalo.R;
import com.marketkhoone.foodmarket.buffalo.adapter.BasketAdapter;
import com.marketkhoone.foodmarket.buffalo.adapter.CategoryAdapter;
import com.marketkhoone.foodmarket.buffalo.adapter.ProductAdapter;
import com.marketkhoone.foodmarket.buffalo.helper.ProgressHelper;
import com.marketkhoone.foodmarket.buffalo.models.CategoryProduct;
import com.marketkhoone.foodmarket.buffalo.models.EditFactor;
import com.marketkhoone.foodmarket.buffalo.models.EditFactorDetail;
import com.marketkhoone.foodmarket.buffalo.models.Product;
import com.marketkhoone.foodmarket.buffalo.models.Setting;
import com.marketkhoone.foodmarket.buffalo.network.MarketkhooneProvider;
import com.marketkhoone.foodmarket.buffalo.network.MarketkhooneService;
import com.marketkhoone.foodmarket.buffalo.ui.main.menu.LoginOrRegisterFragment;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderFragment extends Fragment implements View.OnClickListener, CategoryClickListener,ProductClickListener {
    private MarketkhooneService marketkhooneService;
    private View rootView;

    RecyclerView categoryRecyclerView;
    CategoryAdapter categoryAdapter;
    RecyclerView productRecyclerView;
    ProductAdapter productAdapter;
    RecyclerView basketRecyclerView;
    BasketAdapter basketAdapter;
    private TextView totalFactorTextView;
    private Button orderButton;
    FloatingActionButton searchFab;


    private List<CategoryProduct> categoryProducts = new ArrayList<>();
    private List<Product> products = new ArrayList<>();
    private List<EditFactorDetail> editFactorDetails = new ArrayList<>();
    private EditFactor editFactor = new EditFactor();
    private Setting setting = new Setting();

    CountDownTimer countDownTimer;
    boolean flagTimer;

    String currentCategoryProduct_id = "";
    int lastSkipNumber = 0;
    boolean isLoading = false;
    boolean hasProduct = true;

    public OrderFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_order, container, false);

        categoryRecyclerView = rootView.findViewById(R.id.category_recyclerView);
        productRecyclerView = rootView.findViewById(R.id.product_recyclerView);
        productRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && searchFab.getVisibility() == View.VISIBLE) {
                    searchFab.hide();
                } else if (dy < 0 && searchFab.getVisibility() != View.VISIBLE) {
                    searchFab.show();
                }
            }});
        basketRecyclerView = rootView.findViewById(R.id.basket_recyclerView);
        totalFactorTextView = rootView.findViewById(R.id.totalFactor_textView);
        orderButton = rootView.findViewById(R.id.order_button);
        orderButton.setOnClickListener(this);
        searchFab = rootView.findViewById(R.id.search_fab);
        searchFab.setOnClickListener(this);
        // Inflate the layout for this fragment
        initView();

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null){
            countDownTimer.cancel();
        }
    }

    private void initView(){

        MarketkhooneProvider provider = new MarketkhooneProvider();
        marketkhooneService = provider.getTService();

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("CATEGORYPRODUCT", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpreferences.edit();
        final Gson gson = new Gson();

        Type typeCategoryProduct = new TypeToken<List<CategoryProduct>>() {
        }.getType();
        categoryProducts = gson.fromJson(sharedpreferences.getString("CategoryProduct", ""), typeCategoryProduct);

        products = new ArrayList<>();

//        if(categoryProducts.get(0).products.get(0).title == null){


        if(categoryProducts.get(0).products.size() > 0){
//            List<Product> sliceProducts = categoryProducts.get(0).products.subList(0,20);
//            products = sliceProducts;

            if(categoryProducts.get(0).products.size() >= 20){
                for(int i=0;i<20;i++){
                    products.add(categoryProducts.get(0).products.get(i));
                }
            }else{
                for(int i=0;i<categoryProducts.get(0).products.size();i++){
                    products.add(categoryProducts.get(0).products.get(i));
                }
            }

            currentCategoryProduct_id = categoryProducts.get(0)._id;

            createProduct();
        }else{
            StartTimer();
            isLoading = true;
            getProducts(categoryProducts.get(0)._id,0,20);
        }
//        }else{
//
//        }
//        products = categoryProducts.get(0).products;

        createTotalFactor();
        createCategoryProduct();
        createBasket();
    }

    private void StartTimer() {
        flagTimer=true;
//        retryButton.setVisibility(View.GONE);

        countDownTimer = new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                ProgressHelper.cafeBarError(getActivity(),"اتصال به اینترنت ضعیف یا قطع می باشد. لطفا دوباره سعی کنید");
//                progress.dismiss();
                flagTimer=false;
                isLoading = false;
//                retryButton.setVisibility(View.VISIBLE);
            }

        }.start();
    }

    private void getProducts(String categoryProduct_id, int skip, int limit){
        currentCategoryProduct_id = categoryProduct_id;
        Product product = new Product();
        product.title="";
        product._id="";
        product.available = true;
        product.discount=0;
        product.imageUrl="";
        product.ingredients= new ArrayList<>();
        product.price = -1;
        product.priority = -1;

        products.add(product);
        if(skip == 0){
            createProduct();
        }else{
            productAdapter.notifyItemInserted(products.size() - 1);
        }
        if (flagTimer) {
            final SharedPreferences sharedpreferences = getActivity().getSharedPreferences("CATEGORYPRODUCT", Context.MODE_PRIVATE);
            final SharedPreferences.Editor editor = sharedpreferences.edit();
            final Gson gson = new Gson();
            Call<List<Product>> address_call = marketkhooneService.getProductsBySkipLimit(categoryProduct_id, skip, limit);
            address_call.enqueue(new Callback<List<Product>>() {
                @Override
                public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {

                    if (response.isSuccessful()) {

//                        retryView.setVisibility(View.GONE);
//                        scrollView.setVisibility(View.VISIBLE);
                        List<Product> productsResponse = response.body();

                        if(response.body().size() == 0){
                            hasProduct = false;
                        }

                        for (CategoryProduct categoryProduct:categoryProducts ) {
                            if(categoryProduct._id == categoryProduct_id){
                                for (Product product: response.body()) {
                                    categoryProduct.products.add(product);
                                }
                            }
                        }

                        String json = gson.toJson(categoryProducts);

                        editor.putString("CategoryProduct", json);
                        editor.commit();

                        products.remove(products.size() - 1);
                        int scrollPosition = products.size();
                        productAdapter.notifyItemRemoved(scrollPosition);

                        isLoading = false;
                        if(skip > 0){
                            products.addAll(productsResponse);
                            productAdapter.notifyDataSetChanged();
                        }else{
                            products = productsResponse;
                            createProduct();
                        }

                        countDownTimer.cancel();

//                        progress.dismiss();
                    } else {
                        if (response.code() == 403 || response.code() == 401) {
                            countDownTimer.cancel();

//                            progress.dismiss();

                            ProgressHelper.logout(getActivity());

                            ProgressHelper.cafeBarError(getActivity(),"لطفا دوباره وارد شوید");

                        } else if(response.code() == 500){
                            countDownTimer.cancel();

//                            progress.dismiss();
                            ProgressHelper.showErrorMessage(getActivity(),response.errorBody());

                        } else if(response.code() == 404) {
                            countDownTimer.cancel();
//                            progress.dismiss();
                            ProgressHelper.cafeBarError(getActivity(),"اطلاعات وارد شده اشتباه است.");
                        } else {
                            getProducts(categoryProduct_id, skip, limit);
                        }
                    }
                }
                @Override
                public void onFailure(Call<List<Product>> call, Throwable t) {

                    getProducts(categoryProduct_id, skip, limit);
                }
            });
        }
    }

    private void initScrollListener() {
        productRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == products.size() - 1) {
                        //bottom of list!
                        isLoading = true;
                        loadMore();
                    }
                }
            }
        });


    }

    private void loadMore() {
        if(products.size() == 1){
            lastSkipNumber = 0;
        }else{
            lastSkipNumber = products.size();
        }

        for (CategoryProduct categoryProduct:categoryProducts) {
            if(categoryProduct._id == currentCategoryProduct_id){
                if(categoryProduct.products.size() > lastSkipNumber){

//                    products.remove(products.size() - 1);
//                    int scrollPosition = products.size();
//                    productAdapter.notifyItemRemoved(scrollPosition);
//                    List<Product> sliceProducts = categoryProduct.products.subList(lastSkipNumber,lastSkipNumber + 20);
//                    products.addAll(sliceProducts);

                    if (categoryProduct.products.size() >= lastSkipNumber + 20){
                        for(int i=lastSkipNumber;i<lastSkipNumber + 20;i++){
                            products.add(categoryProduct.products.get(i));
                        }
                    }else{
                        for(int i=lastSkipNumber;i<categoryProduct.products.size();i++){
                            products.add(categoryProduct.products.get(i));
                        }
                    }


                    productAdapter.notifyDataSetChanged();
                    isLoading = false;
                }else{
                    if(lastSkipNumber % 20 == 0 ){
                        if(hasProduct){
                            StartTimer();
                            isLoading = true;
                            getProducts(currentCategoryProduct_id,lastSkipNumber,20);
                        }
                    }
                }
            }
        }


    }

    private void createTotalFactor(){
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("FACTOR", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpreferences.edit();
        final Gson gson = new Gson();

        Type typeEditFactor = new TypeToken<EditFactor>() {
        }.getType();
        editFactor = gson.fromJson(sharedpreferences.getString("EditFactor", ""), typeEditFactor);

        ArrayList<EditFactorDetail> positions = new ArrayList<>();
        int index = 0;
        for (CategoryProduct categoryProduct: categoryProducts) {
            for (Product product:products) {
                if(product.available == false){
                    for (EditFactorDetail factorDetail: editFactor.factorDetails) {
                        if(product._id.contains(factorDetail.productId)){
                            positions.add(factorDetail);
                        }
                    }
                }
            }
        }

        boolean notAvailableFlag = false;
        for (EditFactorDetail e: positions) {
            editFactor.factorDetails.remove(e);
            notAvailableFlag = true;
        }

        if (notAvailableFlag){

            String json = gson.toJson(editFactor);
            editor.putString("EditFactor", json);
            editor.commit();
        }

        editFactorDetails = editFactor.factorDetails;
        long totalFactor = 0;
        for (EditFactorDetail e : editFactor.factorDetails) {
            if(e.discount == null){
                e.discount = "0";
            }
            totalFactor += (e.count * (Long.valueOf(e.price) - Long.valueOf(e.discount)));
        }

        DecimalFormat formatter = new DecimalFormat("#,###,###");
        totalFactorTextView.setText(formatter.format(totalFactor) + " تومان");

    }

    private void createCategoryProduct(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, true);

        categoryRecyclerView.setLayoutManager(layoutManager);

        categoryAdapter = new CategoryAdapter(getActivity(),categoryProducts,this);
        categoryRecyclerView.setAdapter(categoryAdapter);

    }

    private void createProduct(){

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false);
        productRecyclerView.setLayoutManager(layoutManager);
        productAdapter = new ProductAdapter(products,getActivity(),this);
        productRecyclerView.setAdapter(productAdapter);

        productRecyclerView.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {

                    @Override
                    public boolean onPreDraw() {
                        productRecyclerView.getViewTreeObserver().removeOnPreDrawListener(this);

                        for (int i = 0; i < productRecyclerView.getChildCount(); i++) {
                            View v = productRecyclerView.getChildAt(i);
                            ProgressBar imageBorderPprogressBar = v.findViewById(R.id.imageBorder_progressBar);
                            ImageView imageImageView = v.findViewById(R.id.image_imageView);
                            TextView titleTextView = v.findViewById(R.id.title_textView);
                            TextView priceTextView = v.findViewById(R.id.price_textView);

                            imageBorderPprogressBar.setAlpha(0.0f);
                            imageBorderPprogressBar.setTranslationY(+  v.getHeight());
                            imageBorderPprogressBar.animate().alpha(1.0f)
                                    .translationYBy(- v.getHeight())
                                    .setDuration(400)
                                    .setStartDelay(i * 100)
                                    .start();

                            imageImageView.setAlpha(0.0f);
                            imageImageView.setTranslationY(+  v.getHeight());
                            imageImageView.animate().alpha(1.0f)
                                    .translationYBy(- v.getHeight())
                                    .setDuration(400)
                                    .setStartDelay(i * 100)
                                    .start();

                            titleTextView.setAlpha(0.0f);
                            titleTextView.setTranslationY(+  v.getHeight());
                            titleTextView.animate().alpha(1.0f)
                                    .translationYBy(- v.getHeight())
                                    .setDuration(400)
                                    .setStartDelay(i * 100)
                                    .start();

                            priceTextView.setAlpha(0.0f);
                            priceTextView.setTranslationY(+  v.getHeight());
                            priceTextView.animate().alpha(1.0f)
                                    .translationYBy(- v.getHeight())
                                    .setDuration(400)
                                    .setStartDelay(i * 100)
                                    .start();
                        }

                        return true;
                    }
                });

        initScrollListener();
    }

    private void createBasket(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false);
        basketRecyclerView.setLayoutManager(layoutManager);
        basketAdapter = new BasketAdapter(editFactorDetails,getActivity());
        basketRecyclerView.setAdapter(basketAdapter);
    }

    private void orderButtonClick(){
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("USER", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpreferences.edit();

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        if(sharedpreferences.getBoolean("UserLogin", false)){
            SharedPreferences sharedpreferencesSetting = getActivity().getSharedPreferences("SETTING", Context.MODE_PRIVATE);
            final SharedPreferences.Editor editorSetting = sharedpreferencesSetting.edit();
            final Gson gson = new Gson();

            SharedPreferences sharedpreferencesFactor = getActivity().getSharedPreferences("FACTOR", Context.MODE_PRIVATE);
            final SharedPreferences.Editor editorFactor = sharedpreferencesFactor.edit();

            Type typeSetting = new TypeToken<Setting>() {
            }.getType();
            setting = gson.fromJson(sharedpreferencesSetting.getString("Setting", ""), typeSetting);

            Type typeEditFactor = new TypeToken<EditFactor>() {
            }.getType();
            editFactor = gson.fromJson(sharedpreferencesFactor.getString("EditFactor", ""), typeEditFactor);

            long totalFactor = 0;
            for (EditFactorDetail e : editFactor.factorDetails) {
                totalFactor += (e.count * (Long.valueOf(e.price) - Long.valueOf(e.discount)));
            }

            if(totalFactor == 0){
                ProgressHelper.cafeBarDanger(getActivity(),"سبد خرید خالی است");
            }else if(totalFactor < setting.minOrder){
                ProgressHelper.cafeBarDanger(getActivity(),"سبد خرید کمتر از حد اقل سفارش می باشد");
            }else{
                fragmentManager.beginTransaction().replace(R.id.container, new AddressFragment(), "address").addToBackStack("address").commit();
            }

        }else{

            fragmentManager.beginTransaction().replace(R.id.container, new LoginOrRegisterFragment(), "login_or_register").addToBackStack("login_or_register").commit();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.order_button:
                orderButtonClick();
                break;
            case R.id.search_fab:
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                fragmentManager.beginTransaction().replace(R.id.container, new SearchFragment(), "search").addToBackStack("search").commit();
                break;
        }
    }

    @Override
    public void categoryListClicked(int position) {
        products = new ArrayList<>();
        isLoading = false;
        for (CategoryProduct categoryProduct:categoryProducts) {
            if(categoryProduct._id == categoryProducts.get(position)._id){
                currentCategoryProduct_id = categoryProduct._id;
                lastSkipNumber = 0;
                if(categoryProduct.products.size() > 0){
//                    List<Product> sliceProducts = categoryProduct.products.subList(0,20);
//                    products = sliceProducts;

                    if(categoryProduct.products.size() >= 20){
                        for(int i=0;i<20;i++){
                            products.add(categoryProduct.products.get(i));
                        }
                    }else{
                        for(int i=0;i<categoryProduct.products.size();i++){
                            products.add(categoryProduct.products.get(i));
                        }
                    }

                    isLoading = false;
                    productAdapter = new ProductAdapter(products,getActivity(),this);
                    productRecyclerView.setAdapter(productAdapter);

                    productRecyclerView.getViewTreeObserver().addOnPreDrawListener(
                            new ViewTreeObserver.OnPreDrawListener() {

                                @Override
                                public boolean onPreDraw() {
                                    productRecyclerView.getViewTreeObserver().removeOnPreDrawListener(this);

                                    for (int i = 0; i < productRecyclerView.getChildCount(); i++) {
                                        View v = productRecyclerView.getChildAt(i);
                                        ProgressBar imageBorderPprogressBar = v.findViewById(R.id.imageBorder_progressBar);
                                        ImageView imageImageView = v.findViewById(R.id.image_imageView);
                                        TextView titleTextView = v.findViewById(R.id.title_textView);
                                        TextView priceTextView = v.findViewById(R.id.price_textView);

                                        imageBorderPprogressBar.setAlpha(0.0f);
                                        imageBorderPprogressBar.setTranslationY(+  v.getHeight());
                                        imageBorderPprogressBar.animate().alpha(1.0f)
                                                .translationYBy(- v.getHeight())
                                                .setDuration(400)
                                                .setStartDelay(i * 100)
                                                .start();

                                        imageImageView.setAlpha(0.0f);
                                        imageImageView.setTranslationY(+  v.getHeight());
                                        imageImageView.animate().alpha(1.0f)
                                                .translationYBy(- v.getHeight())
                                                .setDuration(400)
                                                .setStartDelay(i * 100)
                                                .start();

                                        titleTextView.setAlpha(0.0f);
                                        titleTextView.setTranslationY(+  v.getHeight());
                                        titleTextView.animate().alpha(1.0f)
                                                .translationYBy(- v.getHeight())
                                                .setDuration(400)
                                                .setStartDelay(i * 100)
                                                .start();

                                        priceTextView.setAlpha(0.0f);
                                        priceTextView.setTranslationY(+  v.getHeight());
                                        priceTextView.animate().alpha(1.0f)
                                                .translationYBy(- v.getHeight())
                                                .setDuration(400)
                                                .setStartDelay(i * 100)
                                                .start();
                                    }

                                    return true;
                                }
                            });
                }else{
                    StartTimer();
                    isLoading = true;
                    getProducts(categoryProducts.get(position)._id,0,20);
                }
            }
        }


    }

    @Override
    public void productListClicked(String productId, boolean isAdd, long totalFactorLast, long totalFactorNow) {
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("FACTOR", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpreferences.edit();
        final Gson gson = new Gson();

        Type typeEditFactor = new TypeToken<EditFactor>() {
        }.getType();
        editFactor = gson.fromJson(sharedpreferences.getString("EditFactor", ""), typeEditFactor);

        editFactorDetails = editFactor.factorDetails;

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(totalFactorLast, totalFactorNow);
        valueAnimator.setDuration(400);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                DecimalFormat formatter = new DecimalFormat("#,###,###");

                totalFactorTextView.setText(formatter.format(valueAnimator.getAnimatedValue()) + " تومان");
            }
        });
        valueAnimator.start();

        if(isAdd){
            basketAdapter = new BasketAdapter(editFactorDetails,getActivity());
            basketRecyclerView.setAdapter(basketAdapter);

            basketRecyclerView.getViewTreeObserver().addOnPreDrawListener(
                    new ViewTreeObserver.OnPreDrawListener() {

                        @Override
                        public boolean onPreDraw() {
                            basketRecyclerView.getViewTreeObserver().removeOnPreDrawListener(this);

                            EditFactorDetail currentEditFactorDetail = new EditFactorDetail();

                            int position = -1;
                            for (int i = 0; i < editFactorDetails.size(); i++) {
                                if(editFactorDetails.get(i).productId.contains(productId)){
                                    position = i;
                                    currentEditFactorDetail = editFactorDetails.get(i);
                                }
                            }

                            View v = basketRecyclerView.getChildAt(position);
                            if(v != null){
                                CardView cardview = v.findViewById(R.id.cardView);
                                TextView borderTextview = v.findViewById(R.id.border_textView);
                                TextView titleTextview = v.findViewById(R.id.title_textView);
                                TextView countTextview = v.findViewById(R.id.count_textView);

                                if(currentEditFactorDetail.count == 1){
                                    cardview.setAlpha(0.0f);
                                    cardview.setTranslationY(+  v.getHeight());
                                    cardview.animate().alpha(1.0f)
                                            .translationYBy(- v.getHeight())
                                            .setDuration(400)
                                            .setStartDelay(0)
                                            .start();

                                    borderTextview.setAlpha(0.0f);
                                    borderTextview.setTranslationY(+  v.getHeight());
                                    borderTextview.animate().alpha(1.0f)
                                            .translationYBy(- v.getHeight())
                                            .setDuration(400)
                                            .setStartDelay(0)
                                            .start();

                                    titleTextview.setAlpha(0.0f);
                                    titleTextview.setTranslationY(+  v.getHeight());
                                    titleTextview.animate().alpha(1.0f)
                                            .translationYBy(- v.getHeight())
                                            .setDuration(400)
                                            .setStartDelay(200)
                                            .start();
                                }else{
                                    countTextview.setAlpha(0.0f);
                                    countTextview.animate().alpha(1.0f)
                                            .setDuration(400)
                                            .setStartDelay(0)
                                            .start();
                                }
                            }

                            return true;
                        }
                    });
        }else{
            basketAdapter = new BasketAdapter(editFactorDetails,getActivity());
            basketRecyclerView.setAdapter(basketAdapter);

            basketRecyclerView.getViewTreeObserver().addOnPreDrawListener(
                    new ViewTreeObserver.OnPreDrawListener() {

                        @Override
                        public boolean onPreDraw() {
                            basketRecyclerView.getViewTreeObserver().removeOnPreDrawListener(this);
                            int position = -1;
                            for (int i = 0; i < editFactorDetails.size(); i++) {
                                if(editFactorDetails.get(i).productId.contains(productId)){
                                    position = i;
                                }
                            }


                            if(!(position == -1)){
                                View v = basketRecyclerView.getChildAt(position);
                                TextView countTextview = v.findViewById(R.id.count_textView);

                                countTextview.setAlpha(0.0f);
                                countTextview.animate().alpha(1.0f)
                                        .setDuration(400)
                                        .setStartDelay(0)
                                        .start();
                            }

                            return true;
                        }
                    });

        }

    }
}
