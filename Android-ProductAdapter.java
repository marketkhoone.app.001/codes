package com.marketkhoone.foodmarket.buffalo.adapter;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.marketkhoone.foodmarket.buffalo.R;
import com.marketkhoone.foodmarket.buffalo.models.EditFactor;
import com.marketkhoone.foodmarket.buffalo.models.EditFactorDetail;
import com.marketkhoone.foodmarket.buffalo.models.EditProduct;
import com.marketkhoone.foodmarket.buffalo.models.FactorDetail;
import com.marketkhoone.foodmarket.buffalo.models.Product;
import com.marketkhoone.foodmarket.buffalo.ui.main.order.ProductClickListener;
import com.marketkhoone.foodmarket.buffalo.ui.main.order.ProductDetailBottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    Context context;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private List<Product> mProducts;
    private EditFactor editFactor;
    private static ProductClickListener itemListener;


    public ProductAdapter(List<Product> products, Context context, ProductClickListener itemListener) {
        this.context = context;
        this.mProducts = products;
        this.itemListener = itemListener;
    }


    @Override
    public ProductAdapter.ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_ITEM) {
//            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View view = inflater.inflate(R.layout.layout_product_listitem, parent, false);
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_product_listitem, parent, false);
            return new ProductViewHolder(view, context, parent);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view, context, parent);
        }
    }

    private class LoadingViewHolder extends ProductAdapter.ProductViewHolder {

        AVLoadingIndicatorView progressBar;

        public LoadingViewHolder(View itemView, Context context, ViewGroup parent) {
            super(itemView,context,parent);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {


        if (holder instanceof ProductViewHolder) {

            SharedPreferences sharedpreferences = context.getSharedPreferences("FACTOR", Context.MODE_PRIVATE);
            final SharedPreferences.Editor editor = sharedpreferences.edit();
            final Gson gson = new Gson();

            Type typeEditFactor = new TypeToken<EditFactor>() {
            }.getType();
            editFactor = gson.fromJson(sharedpreferences.getString("EditFactor", ""), typeEditFactor);

            holder.setProduct(mProducts.get(position), position, context);
        } else if (holder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) holder, position);
        }
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (mProducts.get(position).price == -1 && mProducts.get(position).priority == -1) ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        View itemView;

        ArrayList<Product> products = new ArrayList<>();
        Product product;

        int position;

        private ProgressBar imageBorderPprogressBar;
        private ImageView imageImageView;
        private TextView titleTextView;
        private TextView priceTextView;
        private TextView priceWithDiscountTextView;
        private TextView countTextView;
        private  Button plusButton;
        private  Button minusButton;
        private  Button notAvailableButton;

        public ProductViewHolder(View itemView, Context context, ViewGroup parent) {
            super(itemView);

            this.context = context;
            this.itemView = itemView;

            imageBorderPprogressBar = itemView.findViewById(R.id.imageBorder_progressBar);
            imageImageView = itemView.findViewById(R.id.image_imageView);
            imageImageView.setOnClickListener(this);
            titleTextView = itemView.findViewById(R.id.title_textView);
            priceTextView = itemView.findViewById(R.id.price_textView);
            priceWithDiscountTextView = itemView.findViewById(R.id.price_with_discount_textView);
            countTextView = itemView.findViewById(R.id.count_textView);
            plusButton = itemView.findViewById(R.id.plus_button);
            plusButton.setOnClickListener(this);
            minusButton = itemView.findViewById(R.id.minus_button);
            minusButton.setOnClickListener(this);

            notAvailableButton = itemView.findViewById(R.id.not_available_button);

        }

        public void setProduct(Product product, int position, Context context) {

            this.context = context;
            this.product = product;
            this.position = position;
            product.imageUrl = product.imageUrl;

            if(product.imageUrl.length() > 1){
                Picasso.get()
                        .load(product.imageUrl)
                        .error(R.drawable.logo)
                        .into(imageImageView);
            }else{
                Picasso.get()
                        .load(R.drawable.logo)
                        .error(R.drawable.logo)
                        .into(imageImageView);
            }

            titleTextView.setText(product.title);

            DecimalFormat formatter = new DecimalFormat("#,###,###");
            double price = product.price;
            double discount = product.discount;

            priceTextView.setText(formatter.format(price));

            if(product.discount > 0){
                priceWithDiscountTextView.setVisibility(View.VISIBLE);
                priceTextView.setPaintFlags(priceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                priceWithDiscountTextView.setText(formatter.format(price - discount));

            }else{
                priceWithDiscountTextView.setVisibility(View.GONE);
            }

            if(product.available == false){
                minusButton.setEnabled(false);
                plusButton.setEnabled(false);
                countTextView.setEnabled(false);
                notAvailableButton.setVisibility(View.VISIBLE);
            }else{
                minusButton.setEnabled(true);
                plusButton.setEnabled(true);
                countTextView.setEnabled(true);
                notAvailableButton.setVisibility(View.GONE);
            }
            boolean flagFind = false;

//            ArrayList<EditFactorDetail> positions = new ArrayList<>();
//            int index = 0;
            for (EditFactorDetail e : editFactor.factorDetails) {
                if(e.productId.contains(product._id)){
                    if(product.available == false){
//                        positions.add(e);
                    }else{
                        flagFind = true;
                        countTextView.setText(String.valueOf(e.count));
                        countTextView.setTextColor(context.getResources().getColor(R.color.fontOtherColor));

                        ValueAnimator anim = new ValueAnimator();
                        anim.setIntValues(0, 100);
                        anim.setEvaluator(new ArgbEvaluator());
                        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                            @Override
                            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                int value = (int) valueAnimator.getAnimatedValue();
                                imageBorderPprogressBar.setProgress(value);
                            }
                        });

                        anim.setDuration(400);
                        anim.start();
                    }
                }
//                index++;
            }
//            boolean notAvailableFlag = false;
//            for (EditFactorDetail e: positions) {
//                editFactor.factorDetails.remove(e);
//                notAvailableFlag = true;
//            }
//
//            if (notAvailableFlag){
//                SharedPreferences sharedpreferences = context.getSharedPreferences("FACTOR", Context.MODE_PRIVATE);
//                final SharedPreferences.Editor editor = sharedpreferences.edit();
//                final Gson gson = new Gson();
//
//                String json = gson.toJson(editFactor);
//                editor.putString("EditFactor", json);
//                editor.commit();
//            }

            if(!flagFind){
                countTextView.setText(String.valueOf(0));
                countTextView.setTextColor(context.getResources().getColor(R.color.secondaryFontOtherColor));
            }
        }
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.plus_button:
                    PlusAction();
                    break;
                case R.id.minus_button:
                    MinusAction();
                    break;
                case R.id.image_imageView:
//                    ProductDetailBottomSheetDialog bottomSheetDialog = new ProductDetailBottomSheetDialog();
//                    bottomSheetDialog.show(((Activity) context).getFragmentManager(),"aaaa");

                    FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
                    Bundle arguments = new Bundle();
                    final Gson gson = new Gson();
                    String json = gson.toJson(product);

                    arguments.putString("Product", json);

                    ProductDetailBottomSheetDialog bottomSheetDialog = new ProductDetailBottomSheetDialog();
                    bottomSheetDialog.setArguments(arguments);
                    bottomSheetDialog.show(fragmentManager, "ProductDetail");
                    break;
            }
        }

        public void PlusAction(){
            countTextView.setText(String.valueOf(Integer.valueOf(countTextView.getText().toString()) + 1));
            countTextView.setTextColor(context.getResources().getColor(R.color.fontOtherColor));

            if(Integer.valueOf(countTextView.getText().toString()) == 1){
                ValueAnimator anim = new ValueAnimator();
                anim.setIntValues(0, 100);
                anim.setEvaluator(new ArgbEvaluator());
                anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        int value = (int) valueAnimator.getAnimatedValue();
                        imageBorderPprogressBar.setProgress(value);
                    }
                });

                anim.setDuration(400);
                anim.start();
            }

            SharedPreferences sharedpreferences = context.getSharedPreferences("FACTOR", Context.MODE_PRIVATE);
            final SharedPreferences.Editor editor = sharedpreferences.edit();
            final Gson gson = new Gson();

            Type typeEditFactor = new TypeToken<EditFactor>() {
            }.getType();
            editFactor = gson.fromJson(sharedpreferences.getString("EditFactor", ""), typeEditFactor);

            boolean flagFind = false;
            long totalFactorLast = 0;
            long totalFactorNow = 0;
            for (EditFactorDetail e : editFactor.factorDetails) {
                if(e.discount == null){
                    e.discount = "0";
                }
                totalFactorLast += (e.count * (Long.valueOf(e.price) - Long.valueOf(e.discount)));
                totalFactorNow += (e.count * (Long.valueOf(e.price) - Long.valueOf(e.discount)));
                if(e.productId.contains(product._id)){
                    e.count += 1;
                    totalFactorNow += (Long.valueOf(e.price) - Long.valueOf(e.discount));
                    flagFind = true;
                }
            }

            if(!flagFind){
                EditFactorDetail editFactorDetail = new EditFactorDetail();
                editFactorDetail.count = 1;

                EditProduct editProduct = new EditProduct();
                editProduct._id = product._id;
                editProduct.imageUrl = product.imageUrl;
                editProduct.price = product.price;
                editProduct.discount = product.discount;
                editProduct.priority = product.priority;
                editProduct.title = product.title;
                editFactorDetail.product = editProduct;

                editFactorDetail.productId = product._id;
                editFactorDetail.price = String.valueOf(product.price);
                editFactorDetail.discount = String.valueOf(product.discount);
                editFactorDetail.title = product.title;
                totalFactorNow += (product.price - product.discount);
                editFactor.factorDetails.add(editFactorDetail);
            }

            String json = gson.toJson(editFactor);
            editor.putString("EditFactor", json);
            editor.commit();

            itemListener.productListClicked(product._id,true,totalFactorLast,totalFactorNow);

        }

        public void MinusAction(){
            int count = Integer.valueOf(countTextView.getText().toString());
            if(count > 0) {
                countTextView.setText(String.valueOf(Integer.valueOf(countTextView.getText().toString()) - 1));
            }

            if(count == 1){
                countTextView.setTextColor(context.getResources().getColor(R.color.fontOtherColor));
                ValueAnimator anim = new ValueAnimator();
                anim.setIntValues(100, 0);
                anim.setEvaluator(new ArgbEvaluator());
                anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        int value = (int) valueAnimator.getAnimatedValue();
                        imageBorderPprogressBar.setProgress(value);
                    }
                });

                anim.setDuration(400);
                anim.start();
            }

            SharedPreferences sharedpreferences = context.getSharedPreferences("FACTOR", Context.MODE_PRIVATE);
            final SharedPreferences.Editor editor = sharedpreferences.edit();
            final Gson gson = new Gson();

            Type typeEditFactor = new TypeToken<EditFactor>() {
            }.getType();
            editFactor = gson.fromJson(sharedpreferences.getString("EditFactor", ""), typeEditFactor);

            if(editFactor.factorDetails.size() > 0){
                long totalFactorLast = 0;
                long totalFactorNow = 0;
                EditFactorDetail removeValue = new EditFactorDetail();
                boolean flagZero = false;

                for (int i = 0; i < editFactor.factorDetails.size(); i++) {
                    EditFactorDetail e = editFactor.factorDetails.get(i);
                    if(e.discount == null){
                        e.discount = "0";
                    }
                    totalFactorLast += (e.count * (Long.valueOf(e.price) - Long.valueOf(e.discount)));
                    totalFactorNow += (e.count * (Long.valueOf(e.price) - Long.valueOf(e.discount)));
                    if(e.productId.contains(product._id)){
                        e.count -= 1;
                        totalFactorNow -= (Long.valueOf(e.price) - Long.valueOf(e.discount));

                        if(e.count == 0){
                            removeValue = e;
                            flagZero = true;
                        }
                    }
                }

                if(flagZero){
                    editFactor.factorDetails.remove(removeValue);
                }

                String json = gson.toJson(editFactor);
                editor.putString("EditFactor", json);
                editor.commit();
                itemListener.productListClicked(product._id,false, totalFactorLast, totalFactorNow);
            }
        }
    }
}

